<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
	<title><?php echo $title_for_layout; ?></title>
  <link rel="stylesheet" href="/css/screen.css" media="screen, projection">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:700">
  <?php
    echo $this->fetch('meta');
    echo $this->fetch('css');
  ?>
</head>

<body>

<?php echo $this->element('nav'); ?>

<div id="container">
  <?php echo $this->fetch('content'); ?>
</div>

<script type="text/javascript" src="/js/libs/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/js/libs/knockout-3.0.0.debug.js"></script>
<?php echo $this->Html->script("components/Subjects"); ?>
<?php echo $this->fetch('script'); ?>

</body>
</html>
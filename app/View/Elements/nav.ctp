<nav>
  <ul>
    <li><?php echo $this->Html->link('Home','/'); ?></li>
    <li><?php echo $this->Html->link('Observations',array('controller'=>'observations','action'=>'index')); ?></li>
    <li><?php echo $this->Html->link('Subjects',array('controller'=>'subjects','action'=>'index')); ?></li>
    <li><?php echo $this->Html->link('Faculties',array('controller'=>'faculties','action'=>'index')); ?></li>
    <li><?php echo $this->Html->link('Teachers',array('controller'=>'teachers','action'=>'index')); ?></li>
  </ul>
</nav>

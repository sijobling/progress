  <h1>Teachers</h1>

  <table>
    <thead>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Code</th>
        <th>Subject</th>
        <th>Faculty</th>
      </tr>
    </thead>
    <tbody>
      <!-- ko foreach: teachers -->
      <tr>
        <td><input type="text" name="firstName" placeholder="First Name" data-bind="value: firstName" /></td>
        <td><input type="text" name="lastName" placeholder="Last Name" data-bind="value: lastName" /></td>
        <td><input type="text" name="code" placeholder="Code" data-bind="value: code" /></td>
        <td><input type="text" name="subject" placeholder="Subject" data-bind="value: subject" /></td>
        <td><input type="text" name="faculty" placeholder="Faculty" data-bind="value: faculty" /></td>
      </tr>
      <!-- /ko -->
    </tbody>
  </table>
  
  <button data-bind="click: addTeacher">+ New Teacher</button>

  <pre><?php var_dump($teachers); ?></pre>
<?php
class TeachersController extends Controller {

  public $components = array('RequestHandler');
  var $scaffold;
  
  
  public function index() {
    $this->set('teachers',$this->paginate());
    $this->set('_serialize',array('teachers'));
  }

}

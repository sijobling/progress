<?php
class SubjectsController extends Controller {

  public $components = array('RequestHandler');
  var $scaffold;

  public function index() {
    $this->set('subjects',$this->paginate());
    $this->set('_serialize',array('subjects'));
  }

}

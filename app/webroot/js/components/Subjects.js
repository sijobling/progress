
// Subject model

function Subject(data) {
  var self = this;
  self.id = ko.observable(data ? data.Subject.id : '');
  self.code = ko.observable(data ? data.Subject.code : '');
  self.name = ko.observable(data ? data.Subject.name : '');
  self.faculty = ko.observable(data ? data.Faculty.name : '');
  self.created = ko.observable(data ? data.Subject.created : '');

}

// Subject view model
function SubjectViewModel() {

  // Identify self as View Model
  var self = this;
      
  // Create an observable array for all subjects
  self.subjects = ko.observableArray([]);

  // Load initial state from server
  $.getJSON("/subjects.json", function(data) {
    // Loop through subjects
    var mapped = $.map(data.subjects, function(item) { 
      //  Create Subject object from data
      return new Subject(item);
    });
    // Push subjects to observable array 
    self.subjects(mapped);
  });    
  
  // Operations
  self.addSubject = function() {
    self.subjects.push(new Subject({}));
  };

        
}

ko.applyBindings(new SubjectViewModel());
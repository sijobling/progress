
// Class to represent a Observation

function Observation(data) {
  var self = this;
  this.teacherName = ko.observable(data ? data.teacherName : '');
  this.sessions = ko.observableArray([new Session(data)]);

}

function Session(data) {

  var self = this;

  self.date = ko.observable(data ? data.date : '');
  self.category1 = ko.observable(data ? data.category1 : 0);
  self.category2 = ko.observable(data ? data.category2 : 0);
  self.category3 = ko.observable(data ? data.category3 : 0);
  self.category4 = ko.observable(data ? data.category4 : 0);
  self.category5 = ko.observable(data ? data.category5 : 0);
  
  // Calculate average score from 5 scores
  self.averageScore = ko.computed(function() {
      total = parseInt(self.category1()) + parseInt(self.category2()) + parseInt(self.category3()) + parseInt(self.category4()) + parseInt(self.category5());
      return (total > 0) ? Math.round(total / 5) : 0;
  });

}

// Overall viewmodel for this screen, along with initial state
function ObservationViewModel() {

  var self = this,
      sessionHeaders = $('th.session-header');
      
  self.observations = ko.observableArray([]);

  // Load initial state from server, convert it to Task instances, then populate self.tasks
  $.getJSON("/sample.json", function(allData) {
    var mappedObservations = $.map(allData, function(item) { return new Observation(item) });
    self.observations(mappedObservations);
  });    
  
  // Operations
  self.addObservation = function() {
    self.observations.push(new Observation({}));
  };

  self.addSession = function() {
    var start = new Date(),
        formattedDate;
    formattedDate = start.getDate() + '/' + (start.getMonth()+1) + '/' + start.getFullYear() + ' ' + start.getHours() + ':' + start.getMinutes();
    this.sessions.push(new Session({ 'date': formattedDate }));
    $(sessionHeaders).clone().appendTo('thead tr');
  };

        
}

ko.applyBindings(new ObservationViewModel());

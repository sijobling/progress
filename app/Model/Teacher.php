<?php
App::uses('AppModel', 'Model');

class Teacher extends AppModel {

  public $displayField = 'code';

  public $belongsTo = array(
    'Subject' => array(
      'foreignKey'   => 'subject_id',
    ),
  );

  public $hasMany = array(
    'Observation' => array(
    ),
  );


}
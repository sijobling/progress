<?php
App::uses('AppModel', 'Model');

class Observation extends AppModel {

  public $order = array('Teacher.code ASC','date ASC');
  public $belongsTo = array(
    'Teacher' => array(
    ),
  );

}
<?php
App::uses('AppModel', 'Model');

class Subject extends AppModel {

  public $order ='Subject.code ASC';
  public $belongsTo = array(
    'Faculty' => array(
      'foreignKey'   => 'faculty_id',
    ),
  );

  public $hasMany = array(
    'Teacher' => array(
    ),
  );

}